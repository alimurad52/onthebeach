## Demo
[`https://mystifying-shannon-3e1cd9.netlify.com/`](https://mystifying-shannon-3e1cd9.netlify.com/)

## To run
1. Clone the repository by running `git clone https://alimurad52@bitbucket.org/alimurad52/onthebeach.git`
2. `cd` into the project and run `npm install`
3. While in the folder run `npm start`

## To test
Run `npm test`

## Available routes
`/`

## Available Scripts

`npm start`

`npm test`

`npm run build`

`npm run eject`

