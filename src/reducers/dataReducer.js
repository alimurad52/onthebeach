import {GET_DATA} from "../actions/actionTypes";

const INITIAL_STATE = {
    data: []
};

// to set the data to states depending on the action
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_DATA:
            return Object.assign({}, state, {
                data: action.payload
            });
        default:
            return state;
    }
}