import React from "react";
import SideBarStyles from "../../scss/components/sidebar.module.scss";
import Styles from "../../scss/main.module.scss";
import {Icon, List} from "semantic-ui-react";

class Buttons extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 2,
            sortedAlphabetically: false,
            sortedByPrice: true,
            sortedByStars: false,
            sortIcon: 'sort alphabet ascending',
        };
        this.onClickSortAlphabetically = this.onClickSortAlphabetically.bind(this);
        this.onClickSortByPrice = this.onClickSortByPrice.bind(this);
        this.onClickSortByStars = this.onClickSortByStars.bind(this);
    }

    // sorting alphabetically
    onClickSortAlphabetically() {
        // check if data is sorted already
        if(!this.state.sortedAlphabetically) {
            // resetting other states for sorting to initial state to reset the previously active filters
            this.setState({
                active: 1,
                sortedByPrice: false,
                sortedAlphabetically: true,
                sortedByStars: false
            }, () => this.props.sortAlphabetically());
        }
    }

    // sorting by price
    onClickSortByPrice() {
        // check if data is sorted already
        if(!this.state.sortedByPrice) {
            // resetting other states for sorting to initial state to reset the previously active filters
            this.setState({
                active: 2,
                sortedAlphabetically: false,
                sortedByPrice: true,
                sortedByStars: false
            }, () => this.props.sortByPrice());
        }
    }

    // sorting by rating
    onClickSortByStars() {
        // check if data is sorted already
        if(!this.state.sortedByStars) {
            // resetting other states for sorting to initial state to reset the previously active filters
            this.setState({
                active: 3,
                sortedByStars: true,
                sortedByPrice: false,
                sortedAlphabetically: false
            }, () => this.props.sortByStars());
        }
    }

    render() {
        return (
            <List divided
                verticalAlign='middle'
                  className={`${SideBarStyles.list} ${Styles.mbNone}`}>
                <List.Item id="sort-alphabetically-btn" onClick={this.onClickSortAlphabetically} className={this.state.active === 1 ? SideBarStyles.active : ''}>
                    <List.Content>
                        <List.Header><a href="#!">Sort Alphabetically</a></List.Header>
                        <Icon name={this.state.sortIcon} size='large' />
                    </List.Content>
                </List.Item>
                <List.Item id="sort-price-btn" onClick={this.onClickSortByPrice} className={this.state.active === 2 ? SideBarStyles.active : ''}>
                    <List.Content>
                        <List.Header><a href="#!">Sort by Price</a></List.Header>
                        <Icon name='pound sign' size='large' />
                    </List.Content>
                </List.Item>
                <List.Item id="sort-rating-btn" onClick={this.onClickSortByStars} className={this.state.active === 3 ? SideBarStyles.active : ''}>
                    <List.Content>
                        <List.Header><a href="#!">Sort by Rating</a></List.Header>
                        <Icon name='star outline' size='large' />
                    </List.Content>
                </List.Item>
            </List>
        )
    }
}

export default Buttons;