import React from 'react';
import {Container} from "semantic-ui-react";
import SideBarStyles from './../../scss/components/sidebar.module.scss';
import Buttons from "./Buttons";
import Checkboxes from "./Checkboxes";
import Sliders from "./Sliders";


export class Sidebar extends React.Component{

    render() {
        return (
            <Container className={SideBarStyles.cardMain}>
                <Buttons
                    sortAlphabetically={this.props.sortAlphabetically}
                    sortByPrice={this.props.sortByPrice}
                    sortByStars={this.props.sortByStars} />
                <Checkboxes
                    data={this.props.data}
                    filterByDate={this.props.filterByDate} />
                <Sliders
                    filterByPrice={this.props.filterByPrice} />
            </Container>

        )
    }
}

export default Sidebar;