import React from "react";
import SideBarStyles from "../../scss/components/sidebar.module.scss";
import {Accordion, Grid, Icon} from "semantic-ui-react";
import Styles from "../../scss/main.module.scss";
import {settings, starSettings} from "../../const/const";
import { Range } from 'rc-slider';

class Sliders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            priceFilter: true,
            minVal: settings.min,
            lastVal: settings.max,
            starFilter: true,
            starMinVal: starSettings.min,
            starLastVal: starSettings.max
        };
        this.onChangeSlider = this.onChangeSlider.bind(this);
        this.onChangeStarSlider = this.onChangeStarSlider.bind(this);
    }

    componentDidMount() {
        //to pass onchange listener to the sliders configurations
        settings.onChange = (val) => {
            this.onChangeSlider(val);
        };
        starSettings.onChange = (val) => {
            this.onChangeStarSlider(val);
        };
    }

    //onchange for price slider
    onChangeSlider(e) {
        this.setState({
            minVal: e[0],
            lastVal: e[1],
        }, () => this.props.filterByPrice(e[0], e[1], this.state.starMinVal, this.state.starLastVal));
    }

    //onchange for star slider
    onChangeStarSlider(e) {
        this.setState({
            starMinVal: e[0],
            starLastVal: e[1]
        }, () => this.props.filterByPrice(this.state.minVal, this.state.lastVal, e[0], e[1]));
    }

    render() {
        return (
            <Accordion
                fluid
                styled
                className={SideBarStyles.accordion} >
                <Accordion.Title
                    active={this.state.priceFilter}
                    index={0}
                    className={SideBarStyles.iconsFloatRight}
                    onClick={() => this.setState(prevState => ({priceFilter: !prevState.priceFilter}))}>
                    Price Range
                    <Icon size='large' name={this.state.priceFilter ? 'angle down' : 'angle right'} />
                </Accordion.Title>
                <Accordion.Content active={this.state.priceFilter}>
                    <Grid columns={'equal'}>
                        <Grid.Column className={Styles.textPaddingBottom}>
                            <small>MIN</small><br />
                            <strong>£ {this.state.minVal}</strong>
                        </Grid.Column>
                        <Grid.Column textAlign='right' className={Styles.textPaddingBottom}>
                            <small>MAX</small><br />
                            <strong>£ {this.state.lastVal}</strong>
                        </Grid.Column>
                    </Grid>

                    <Range
                        min={settings.min}
                        max={settings.max}
                        defaultValue={[settings.min, settings.max]}
                        step={settings.step}
                        onChange={this.onChangeSlider}/>

                </Accordion.Content>
                <Accordion.Title
                    active={this.state.starFilter}
                    index={0}
                    className={SideBarStyles.iconsFloatRight}
                    onClick={() => this.setState(prevState => ({starFilter: !prevState.starFilter}))}>
                    Star Range
                    <Icon size='large' name={this.state.starFilter ? 'angle down' : 'angle right'} />
                </Accordion.Title>
                <Accordion.Content
                    active={this.state.starFilter}
                    className={SideBarStyles.starSliderIcons} >
                    <Grid columns={'equal'}>
                        <Grid.Column className={Styles.textPaddingBottom}>
                            <small>MIN</small><br />
                            <strong>{this.state.starMinVal}<Icon name='star' /></strong>
                        </Grid.Column>
                        <Grid.Column textAlign='right' className={Styles.textPaddingBottom}>
                            <small>MAX</small><br />
                            <strong>{this.state.starLastVal}<Icon name='star' /></strong>
                        </Grid.Column>
                    </Grid>
                    <Range
                        min={starSettings.min}
                        max={starSettings.max}
                        defaultValue={[starSettings.min, starSettings.max]}
                        step={starSettings.step}
                        onChange={this.onChangeStarSlider}/>
                </Accordion.Content>
            </Accordion>
        )
    }
}

export default Sliders;