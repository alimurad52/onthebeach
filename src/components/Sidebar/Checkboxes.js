import React from 'react';
import {Accordion, Checkbox, Container, Icon} from "semantic-ui-react";
import SideBarStyles from "../../scss/components/sidebar.module.scss";
import Moment from "react-moment";

class Checkboxes extends React.Component {
    constructor(props) {
        super(props);
        // sets to add the checkbox values to
        this.selectedDateCheckbox = new Set();
        this.selectedAirportCheckbox = new Set();
        this.state = {
            dateFilter: true,
            dateCheckbox: [],
            departureFilter: true,
            departureCheckbox: []
        };
        this.prepareDateFilter = this.prepareDateFilter.bind(this);
        this.onChangeDepartureDate = this.onChangeDepartureDate.bind(this);
        this.onChangeDepartureAirport = this.onChangeDepartureAirport.bind(this);
    }

    componentDidMount() {
        // to prepare the data for the sidebar filter. to remove the duplicates mainly
        this.prepareDateFilter();
    }

    //to remove duplicates if any
    prepareDateFilter() {
        let objDates = {};
        let objAirports = {};
        let datesArr = [];
        let airportArr = [];
        //object with value keys
        this.props.data.map((item) => {
            objDates[item.departure] = 0;
            return objDates[item.departure];
        });
        // adding object keys to array
        Object.keys(objDates).map((item) => {
            datesArr.push(item);
            return datesArr;
        });
        this.props.data.map((item) => {
            objAirports[item.departure_airport] = 0;
            return objAirports[item.departure_airport];
        });
        Object.keys(objAirports).map((item) => {
            airportArr.push(item);
            return airportArr;
        });
        this.setState({
            dateCheckbox: datesArr,
            departureCheckbox: airportArr
        });
    }

    onChangeDepartureDate(item) {
        // upon change to checkbox, check the value if it already exists in the checkbox set
        // if it exists, remove it and if it doesn't add it
        if (this.selectedDateCheckbox.has(item)) {
            this.selectedDateCheckbox.delete(item);
        } else {
            this.selectedDateCheckbox.add(item);
        }
        this.props.filterByDate(this.selectedDateCheckbox);
    }

    onChangeDepartureAirport(item) {
        // upon change to checkbox, check the value if it already exists in the checkbox set
        // if it exists, remove it and if it doesn't add it
        if (this.selectedAirportCheckbox.has(item)) {
            this.selectedAirportCheckbox.delete(item);
        } else {
            this.selectedAirportCheckbox.add(item);
        }
        this.props.filterByDate(this.selectedAirportCheckbox);
    }

    render() {
        return (
            <Accordion fluid styled className={SideBarStyles.accordion}>
                <Accordion.Title
                    active={this.state.dateFilter}
                    index={0}
                    className={SideBarStyles.iconsFloatRight}
                    onClick={() => this.setState(prevState => ({dateFilter: !prevState.dateFilter}))}>
                    Departure Dates
                    <Icon size='large' name={this.state.dateFilter ? 'angle down' : 'angle right'} />
                </Accordion.Title>
                <Accordion.Content active={this.state.dateFilter}>
                    {
                        this.state.dateCheckbox.map((item, i) => {
                            return <Container key={i}><Checkbox className={SideBarStyles.checkboxPositioning} onClick={this.onChangeDepartureDate.bind(null, item)} value={item} label={<label><Moment format={"Do MMM YYYY"}>{new Date(item)}</Moment></label>} /></Container>
                        })
                    }
                </Accordion.Content>
                <Accordion.Title
                    active={this.state.departureFilter}
                    index={0}
                    className={SideBarStyles.iconsFloatRight}
                    onClick={() => this.setState(prevState => ({departureFilter: !prevState.departureFilter}))}>
                    Departure Airport
                    <Icon size='large' name={this.state.departureFilter ? 'angle down' : 'angle right'} />
                </Accordion.Title>
                <Accordion.Content active={this.state.departureFilter}>
                    {
                        this.state.departureCheckbox.map((item, i) => {
                            return <Container key={i}><Checkbox className={SideBarStyles.checkboxPositioning} onClick={this.onChangeDepartureAirport.bind(null, item)} value={item} label={item} /></Container>
                        })
                    }
                </Accordion.Content>
            </Accordion>
        )
    }
}

export default Checkboxes;