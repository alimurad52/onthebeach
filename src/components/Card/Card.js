import React from 'react';
import {Container, Grid, Image, Header, Rating, Button, Accordion, Icon} from "semantic-ui-react";
import Styles from '../../scss/main.module.scss';
import CardStyles from '../../scss/components/card.module.scss';
import Moment from 'react-moment';
import {formatter} from "../../const/const";

class Card extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false
        };
    }

    render() {
        return (
            <Container className={CardStyles.cardMain}>
                <Grid stackable>
                    <Grid.Column className={`${Styles.pbNone} ${Styles.ptNone}`} width={10}>
                        <Image src={require('./../../assets/'+this.props.image)} fluid />
                    </Grid.Column>
                    <Grid.Column className={`${Styles.containerPaddingRight} ${Styles.plNone}`} width={6}>
                        <Header className={Styles.mbNone} as={'h3'}>{this.props.title}</Header>
                        <Header.Subheader className={Styles.subHeading}>{this.props.location}</Header.Subheader>
                        <Rating className={CardStyles.ratings} defaultRating={this.props.rating} maxRating={this.props.rating} disabled />
                        <div className={CardStyles.hotelDetails}>
                            <p><strong>{this.props.capacity.adults}</strong> Adults, <strong>{this.props.capacity.children}</strong> {this.props.capacity.children > 1 ? 'children' : 'child'} {this.props.capacity.infant > 0 ? <span>& <strong>{this.props.capacity.infant}</strong> infant</span> : ""}</p>
                            <p><strong><Moment format={"Do MMM YYYY"}>{new Date(this.props.departure)}</Moment></strong> for <strong>7 days</strong></p>
                            <p>departing from <strong>{this.props.departureAirport}</strong></p>
                        </div>
                        <Button primary className={`${Styles.maxWidth} ${Styles.buttonMain}`}><p className={Styles.mbNone}>Book now</p><Header className={Styles.mtNone} as={'h2'}>{formatter.format(this.props.price)}</Header></Button>
                    </Grid.Column>
                    <Grid.Row className={`${Styles.ptNone} ${Styles.pbNone}`}>
                        <Grid.Column width={16}>
                            <Accordion>
                                <Accordion.Title
                                    active={this.state.active}
                                    index={0}
                                    onClick={() => this.setState(prevState => ({active: !prevState.active}))}
                                    className={`${CardStyles.titlePosition} ${Styles.textPadding} ${Styles.textColor}`}
                                >
                                    {this.state.active ? <strong>Read less</strong> : <strong>Read more</strong>} about this property
                                    <Icon size='large' name={this.state.active ? 'angle down' : 'angle right'} />
                                </Accordion.Title>
                                <Accordion.Content className={`${Styles.ptNone} ${Styles.textPadding} ${Styles.textPaddingBottom} ${Styles.textPaddingTop}`} active={this.state.active}>
                                    <Header as='h4'>Overview</Header>
                                    <p>{this.props.description}</p>
                                </Accordion.Content>
                            </Accordion>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        )
    }
}

export default Card;