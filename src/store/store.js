import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import reducers from './../reducers/rootReducer';
import storage from 'redux-persist/lib/storage';

// creating the object from which will be passed to local storage
const persistConfig = {
    key: 'OnTheBeach',
    storage
};

const pReducer = persistReducer(persistConfig, reducers);

export const store = createStore(pReducer, applyMiddleware(thunk));
export const persistor = persistStore(store);