import React from 'react';
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import '@testing-library/jest-dom'
import 'jest-enzyme';
import {expect} from 'chai';
import Buttons from "../components/Sidebar/Buttons";

//configure enzyme to use the adapter
configure({ adapter: new Adapter() });

describe('Testing the sidebar components', () => {
    it('should sort hotel data alphabetically', function () {
        const wrapper = shallow(<Buttons sortAlphabetically={()=> {}} />);
        // find the button and simulate click to trigger function
        wrapper.find('#sort-alphabetically-btn').simulate('click');
        expect(wrapper.instance().state.sortedAlphabetically).to.equal(true);
    });
    it('should sort hotel data by price', function () {
        const wrapper = shallow(<Buttons sortByPrice={()=> {}} />);
        // find the button and simulate click to trigger function
        wrapper.find('#sort-price-btn').simulate('click');
        expect(wrapper.instance().state.sortedByPrice).to.equal(true);

    });
    it('should sort hotel data by stars', function () {
        const wrapper = shallow(<Buttons sortByStars={() => {}} />);
        // find the button and simulate click to trigger function
        wrapper.find('#sort-rating-btn').simulate('click');
        expect(wrapper.instance().state.sortedByStars).to.equal(true);
    });
});
