import data from './data';

//starting point of the range slider. should be the cheapest price of all.
export let starting = () => {
    let d = data.data.slice();
    let arr = d.sort((a, b) => {
        return a.price - b.price
    });
    return arr[0].price;
};

//ending point of the range slider. should be the most expensive price of all.
export let last = () => {
    let d = data.data.slice();
    let arr = d.sort((a, b) => {
        return b.price - a.price
    });
    return arr[0].price
};

export let firstStar = () => {
    let d = data.data.slice();
    let arr = d.sort((a, b) => {
        return a.stars - b.stars
    });
    return arr[0].stars;
};

export let lastStar = () => {
    let d = data.data.slice();
    let arr = d.sort((a, b) => {
        return b.stars - a.stars
    });
    return arr[0].stars;
};

//settings for the slider
export const settings = {
    // subtracting and adding 10% of the initial value from the starting and ending point
    // for the user experience to be smooth and the slider not to be too sensitive
    min: Math.round(starting()) - 10/100 * Math.round(starting()),
    max: Math.round(last() + 10/100 * Math.round(last())),
    step: 1
};

export const starSettings = {
    // subtracting and adding 1 to the initial value from starting and ending point
    // for the user experience to be smooth and the slider to not be too sensitive
    min: Math.round(firstStar()),
    max: Math.round(lastStar()),
    step: 1
};

//formatter to format the price to currency
export const formatter = new Intl.NumberFormat('en-UK', {
    style: 'currency',
    currency: 'GBP'
});
