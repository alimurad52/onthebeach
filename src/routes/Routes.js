import React from 'react';
import { Router, Route, withRouter } from "react-router-dom";
import history from './history';
import Main from "../layout/Main";


const base_url = process.env.PUBLIC_URL;
//assigning history to window for pushing any route if needed
window.routerHistory = history;

const Routes = () => {
    return (
        <Router history={history}>
            <Route exact path={`${base_url}/`} component={withRouter(Main)} />
        </Router>
    )
};

export default Routes;
