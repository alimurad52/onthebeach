import {GET_DATA} from "./actionTypes";
import data from './../const/data';

export const getData = () => {
    return (dispatch) => {
        dispatch(setData(data.data));
    }
};

function setData(res) {
    return {
        type: GET_DATA,
        payload: res
    }
}