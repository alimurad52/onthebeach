import React from 'react';
import {Container, Grid} from "semantic-ui-react";
import Sidebar from "./../components/Sidebar/Sidebar";
import Card from "./../components/Card/Card";
import data from './../const/data';
import Styles from './../scss/main.module.scss';
import Fallback from "./Fallback/Fallback";
import {getData} from "../actions/dataAction";
import {connect} from 'react-redux';


class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data.slice(),
            dataCopy: this.props.data.slice(),
            alphabeticallySorted: false,
            sortedByPrice: false,
            sortedByStars: false
        };
        this.sortAlphabetically = this.sortAlphabetically.bind(this);
        this.sortByPrice = this.sortByPrice.bind(this);
        this.sortByStars = this.sortByStars.bind(this);
        this.filterData = this.filterData.bind(this);
        this.filterPrice = this.filterPrice.bind(this);
        this.filterStar = this.filterStar.bind(this);
        this.resumeSort = this.resumeSort.bind(this);
    }

    componentDidMount() {
        // use redux function to get and set data to store
        this.props.getData();
        // to sort data by price on launch
        this.sortByPrice();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // check if after any props call the data has updated, if updated, set the new data to state
        if(prevProps.data !== this.props.data) {
            this.setState({data: this.props.data.slice(), dataCopy: this.props.data.slice()});
        }
    }

    sortAlphabetically() {
        // check if data is sorted already
        if(!this.state.sortedAlphabetically) {
            let sortedArr = this.state.data.sort((a, b) => {
                if (a.title < b.title) {
                    return -1;
                }
                if (a.title > b.title) {
                    return 1;
                }
                return 0;
            });
            // resetting other states for sorting to initial state to reset the previously active filters
            this.setState({
                data: sortedArr,
                sortedAlphabetically: true,
                sortedByPrice: false,
                sortedByStars: false
            });
        } else {
            // if user clicks second time, set data to initial
            this.setState({data: this.state.dataCopy.slice(), sortedAlphabetically: false});
        }

    }

    sortByPrice() {
        // check if data is sorted already
        if(!this.state.sortedByPrice) {
            let sortedArr = this.state.data.sort((a, b) => {
                return a.price - b.price;
            });
            // resetting other states for sorting to initial state to reset the previously active filters
            this.setState({
                data: sortedArr,
                sortedByPrice: true,
                sortedAlphabetically: false,
                sortedByStars: false
            });
        }
    }

    sortByStars() {
        // check if data is sorted already, if yes, then ignore
        if(!this.state.sortedByStars) {
            let sortedArr = this.state.data.sort((a, b) => {
                return b.stars - a.stars;
            });
            // resetting other states for sorting to initial state to reset the previously active filters
            this.setState({
                data: sortedArr,
                sortedAlphabetically: false,
                sortedByPrice: false,
                sortedByStars: true
            });
        }
    }

    filterData(date) {
        // convert set to array
        let arr = Array.from(date);
        let tempArr = [];
        // check if the array values match the data values
        if(arr.length > 0) {
            for(let i = 0; i < arr.length; i++) {
                for(let k = 0; k < data.data.length; k++) {
                    if(arr[i] === data.data[k].departure || arr[i] === data.data[k].departure_airport) {
                        tempArr.push(data.data[k]);
                    }
                }
            }
            this.setState({data: tempArr});
        } else {
            // if none is selected, set data to initial
            this.setState({data: data.data});
        }
        this.resumeSort();
    }

    //this function would be called for filters as both filters should work palely
    filterPrice(start, end, starStart, starEnd) {
        let arr = [];
        // check if any of the resorts match the price between start and end value
        for(let i = 0; i < this.state.dataCopy.length; i++) {
            if(this.state.dataCopy[i].price >= start && this.state.dataCopy[i].price <= end) {
                arr.push(this.state.dataCopy[i]);
            }
        }
        // always run filter star function to ensure that both filters works side by side
        this.setState({data: arr}, () => this.filterStar(starStart, starEnd));
    }

    filterStar(start, end) {
        let arr = [];
        // check if any of the resorts match the stars between start and end value
        for(let i = 0; i < this.state.data.length; i++) {
            if(this.state.data[i].stars >= start && this.state.data[i].stars <= end) {
                arr.push(this.state.data[i]);
            }
        }
        this.setState({data: arr}, () => this.resumeSort());
    }

    // if any of the filters change while one of the sort option was active
    // sort the data back according to the sort action chosen previously
    resumeSort() {
        if(this.state.sortedByPrice) {
            this.setState({sortedByPrice: false}, () => this.sortByPrice());
        }
        if(this.state.sortedByStars) {
            this.setState({sortedByStars: false}, () => this.sortByStars());
        }
        if(this.state.sortedAlphabetically) {
            this.setState({sortedAlphabetically: false}, () => this.sortAlphabetically());
        }
    }

    render() {
        return (
            <Container className={Styles.mainContainer}>
                <Grid stackable>
                    <Grid.Column className={Styles.ptNone} width={4}>
                        <Sidebar
                            sortAlphabetically={this.sortAlphabetically}
                            sortByPrice={this.sortByPrice}
                            sortByStars={this.sortByStars}
                            data={data.data}
                            filterByDate={this.filterData}
                            filterByPrice={this.filterPrice}
                        />
                    </Grid.Column>
                    <Grid.Column width={12}>
                        {
                            this.state.data.length > 0 ? this.state.data.map((item, i) => {
                                return <Card
                                    key={i}
                                    capacity={item.capacity}
                                    image={item.img}
                                    title={item.title}
                                    rating={item.stars}
                                    location={item.location}
                                    departure={item.departure}
                                    description={item.description}
                                    departureAirport={item.departure_airport}
                                    price={item.price} />
                            }) : <Fallback />
                        }

                    </Grid.Column>
                </Grid>
            </Container>
        )
    }
}
const mapStateToProps = (state) => {
    let data = state.data.data;
    return {
        data
    };
};
const mapDispatchToProps = dispatch => ({
    getData: () => dispatch(getData())
});

export default connect(mapStateToProps, mapDispatchToProps)(Main)