import React from 'react';
import {Container, Image} from "semantic-ui-react";

class Fallback extends React.Component {
    render() {
        return (
            <Container textAlign='center'>
                <Image src={require('./../../assets/empty-placeholder.svg')} size='large' verticalAlign='middle' centered />
                <strong><p>No resorts found matching your filters.</p>
                <p>Please change your filters to see available resorts.</p></strong>
            </Container>
        )
    }
}

export default Fallback;